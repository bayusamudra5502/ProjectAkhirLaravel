<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\ValidationException;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = User::find($request->user()->id);
        return view('auth.profile', ['profile' => $user]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return view('auth.profile', ['profile' => $user]);
    }

    /**
     * Display an editor for edit the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request) {
        $user = User::find($request->user()->id);
        return view('auth.profile-edit', ['profile' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request->validate([
            'name' => ['string', 'max:255'],
            'birthday' => ['date'],
            'photo' => [
                'image',
                'dimensions:ratio=0/0',
                'max:10240'
            ]
        ]);

        $user = User::find($request->user()->id);

        if($request->photo){
            $file = $request->file('photo');
            $name = time(). "_" . str_replace(' ', '_', $file->getClientOriginalName());
            $success = Storage::disk('public')->put("users/$name", file_get_contents($file), );
    
            if(!$success){
                throw ValidationException::withMessages(['photo' => 'Failed to upload photo']);
            }

            $user->photo_url = "users/$name";
        }

        if($request->name) {
            $user->name = $request->name; 
        }

        if($request->birthday) {
            $user->birthday = $request->birthday; 
        }
        
        $user->save();
        return redirect(RouteServiceProvider::PROFILE);
    }
}
