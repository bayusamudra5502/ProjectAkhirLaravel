<?php

namespace App\Http\Controllers\Campaign;

use App\Http\Controllers\Controller;
use App\Models\Campaign;
use App\Models\Donation;
use App\Models\Prayer;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\ValidationException;
use PDF;

class CampaignController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $campaigns = Campaign::all();
        return view('campaign.list', compact('campaigns'));
    }

    /**
     * Display own campaign
     *
     * @return \Illuminate\Http\Response
     */
    public function list_own(Request $request) {
        $campaigns = Campaign::where('user_id', $request->user()->id)->get();
        return view('campaign.own', compact('campaigns'));
    }

    public function add() {
        return view('campaign.add');
    }

    public function edit($id, Request $request) {
        $campaign = Campaign::find($id);

        if($campaign->user_id === $request->user()->id){
            return view('campaign.edit', compact('campaign'));
        }else{
            return back(403);
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'photo' => [
                'required', 'image', 'max:5120'
            ],
            'expired' => ['required', 'date'],
            'description' => ['required', 'string']
        ]);

        if($request->user()->role !== 'fundraiser') {
            throw ValidationException::withMessages(['user' => 'Unathorized user']);
        }

        $campaign = new Campaign;
        $campaign->user_id = $request->user()->id;
        $campaign->name = $request->name;
        $campaign->expired = $request->expired;
        $campaign->description = $request->description;

        $file = $request->file('photo');
        $name = time(). "_" . str_replace(' ', '_', $file->getClientOriginalName());
        $success = Storage::disk('public')->put("campaigns/$name", file_get_contents($file), );

        if(!$success){
            throw ValidationException::withMessages(['photo' => 'Failed to upload photo']);
        }

        $campaign->photo_url = "campaigns/$name";
        $campaign->save();

        return redirect(RouteServiceProvider::CAMPAIGN. "/dashboard");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $campaign = Campaign::find($id);
        $prayers = Prayer::select('prayers.*', 'users.name', 'users.photo_url')
            ->join('users', 'users.id', '=', 'prayers.user_id');

        return view('campaign.details', compact('campaign', 'prayers'));
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => ['string', 'max:255'],
            'photo' => [
                'image', 'size:10240'
            ],
            'expired' => ['date'],
            'description' => ['string']
        ]);

        $campaign = Campaign::find($id);

        if($campaign->user_id !== $request->user()->id){
            throw ValidationException::withMessages(['user' => 'Unathorized user']);
        }

        if($request->name) {
            $campaign->name = $request->name;
        }

        if($request->expired) {
            $campaign->expired = $request->expired;
        }

        if($request->description) {
            $campaign->description = $request->description;
        }

        if($request->photo) {
            $file = $request->file('photo');
            $name = time(). "_" . str_replace(' ', '_', $file->getClientOriginalName());
            $success = Storage::disk('public')->put("campaigns/$name", file_get_contents($file), );
    
            if(!$success){
                throw ValidationException::withMessages(['photo' => 'Failed to upload photo']);
            }

            $campaign->photo_url = "campaigns/$name";
        }

        $campaign->save();
        return redirect(RouteServiceProvider::CAMPAIGN. "/$id");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $campaign = Campaign::find($id);

        if($campaign->user_id !== $request->user()->id){
            throw ValidationException::withMessages(['user' => 'Unathorized user']);
        }

        $campaign->delete();
        return redirect(RouteServiceProvider::CAMPAIGN. "/dashboard");
    }

    public function show_report(Request $request, $id) {
        $campaign = Campaign::find($id);

        if($campaign->user_id !== $request->user()->id){
            throw ValidationException::withMessages(['user' => 'Unathorized user']);
        }

        $total = Donation::where('campaign_id', $id)->sum('amount');
        $count = Donation::where('campaign_id', $id)->count('id');
        $donors = Donation::select(['donations.*', 'users.name'])
            ->join('users', 'users.id', '=', 'donations.user_id')
            ->where('campaign_id', $id)->get();

        return view('campaign.report', compact('id','count','total','donors', 'campaign'));
    }

    public function print_report(Request $request, $id) {
        $campaign = Campaign::find($id);

        if($campaign->user_id !== $request->user()->id){
            throw ValidationException::withMessages(['user' => 'Unathorized user']);
        }

        $total = Donation::where('campaign_id', $id)->sum('amount');
        $count = Donation::where('campaign_id', $id)->count('id');
        $donors = Donation::select(['donations.*', 'users.name'])
            ->join('users', 'users.id', '=', 'donations.user_id')
            ->where('campaign_id', $id)->get();

        $pdf = PDF::loadview('campaign.print', compact('id','count','total','donors', 'campaign'));
        return $pdf->download("report-". $campaign->name . "-" . time(). ".pdf");
        // return view('campaign.print', compact('id','count','total','donors', 'campaign'));
    }
}
