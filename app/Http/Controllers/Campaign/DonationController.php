<?php

namespace App\Http\Controllers\Campaign;

use App\Http\Controllers\Controller;
use App\Models\Donation;
use Illuminate\Http\Request;

class DonationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $donations = Donation::join('campaigns', 'campaigns.id', '=', 'donations.campaign_id')
                        ->where('donations.user_id', $request->user()->id)
                        ->get();
        $total = Donation::where('user_id', $request->user()->id)->sum('amount');
        $number = Donation::where('user_id', $request->user()->id)->count('campaign_id');

        return view('donation.list', compact('donations', 'total', 'number'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $request->validate([
           "amount" => ['required','numeric','gt:10000']
        ]);

        $donation = new Donation;

        $donation->campaign_id = $id;
        $donation->amount = $request->amount;
        $donation->user_id = $request->user()->id;
        
        $donation->save();

        return redirect("/campaigns/$id");
    }

    public function add($id) {
        return view('donation.donate', compact('id'));
    }
}
