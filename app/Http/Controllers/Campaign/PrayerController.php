<?php

namespace App\Http\Controllers\Campaign;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Prayer;
class PrayerController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $request->validate([
            'prayer' => 'required',
        ], [
            'prayer.required' => 'Kolom Doa Tidak Boleh Kosong'
        ]);

        $prayer = new Prayer;
        $prayer->prayer = $request->prayer;
        $prayer->user_id=$request->user()->id;
        $prayer->campaign_id=$id;
        $prayer->is_anonymous='null';
        $prayer->save();

        return back();
    }
}
