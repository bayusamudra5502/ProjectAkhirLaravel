<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $req)
    {
        if($req->user()->role === 'admin') {
            return view('dashboard.admin');
        } else if ($req->user()->role === 'fundraiser') {
            return view('dashboard.fundraiser');
        } else if ($req->user()->role === 'donor') {
            return view('dashboard.donor');
        } else {
            throw new Exception("Role not found", 500);
        }
    }

}
