<?php

namespace App\Http\Middleware;

class DonorAuthentication extends RoleAuthentication
{
    public function isAllowed(string $role)
    {
        return $role === 'donor';
    }
}
