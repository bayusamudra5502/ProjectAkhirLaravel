<?php

namespace App\Http\Middleware;

class CampaignAdmin extends RoleAuthentication
{
    public function isAllowed(string $role)
    {
        return $role === 'admin' || $role === 'fundraiser';
    }
}
