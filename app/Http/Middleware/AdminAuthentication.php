<?php

namespace App\Http\Middleware;

class AdminAuthentication extends RoleAuthentication
{
    public function isAllowed(string $role)
    {
        return $role === 'admin';
    }
}
