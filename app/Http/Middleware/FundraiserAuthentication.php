<?php

namespace App\Http\Middleware;

class FundraiserAuthentication extends RoleAuthentication
{
    public function isAllowed(string $role)
    {
        return $role === 'fundraiser';
    }
}
