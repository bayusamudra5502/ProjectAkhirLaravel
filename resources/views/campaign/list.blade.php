@extends('template.master')

@section('content')
    <div class="my-5">
        <h1 class="text-3xl font-bold">All Campaigns</h1>
    </div>
    <div class="mt-10">
        <div class="flex flex-wrap my-5">
            @forelse ($campaigns as $campaign)
                <div class="max-w-md bg-white rounded-lg border border-gray-200 shadow-md dark:bg-gray-800 dark:border-gray-700">
                    <a href="/campaigns/{{ $campaign->id }}">
                        <img class="rounded-t-lg" src="{{ env('BUCKET_URL') . '/' . $campaign->photo_url }}" alt="">
                    </a>
                    <div class="p-5">
                        <a href="/campaigns/{{ $campaign->id }}">
                            <span class="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">{{ $campaign->name }}</span>
                        </a>
                        <p class="my-3 line-clamp-5 font-normal text-gray-700 dark:text-gray-400">{{ $campaign->description }}</p>
                        <div class="flex gap-2 items-center">
                          <a href="/campaigns/{{ $campaign->id }}" class="inline-flex items-center py-2 px-3 text-sm font-medium text-center text-white bg-blue-700 rounded-lg hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                            Show Details
                          </a>
                         </div>
                    </div>
                </div>
            @empty
                <div class="mx-auto mt-5 text-center">
                <img class="w-32" src="{{ asset('assets/img/campaign.png') }}" alt="" />
                <p class="mt-5 text-2xl font-bold">
                    Oops..
                </p>
                <p class="mt-2">There is no campaigns yet</p>
                </div>
            @endforelse
        </div>
    </div>
@endsection