@extends('template.master')

@section('content')
<div class="mt-3 p-4 pt-5 z-10">
    <div class="mb-6 flex items-center justify-center max-h-96 rounded-lg overflow-hidden">
      <img src="{{ env('BUCKET_URL') . '/' . $campaign->photo_url }}" class="w-full" alt="{{ $campaign->name }} Photo" />
    </div>
    <div class="flex flex-row justify-between">
      <h1 class="font-bold text-3xl">{{ $campaign->name }}</h1>
      @auth
        @if (Auth::user()->id === $campaign->user_id)
          <div class ="flex gap-1">
            <a href="/campaigns/{{ $campaign->id }}/edit" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800">Edit</a>
            <a href="/campaigns/{{ $campaign->id }}/report" class="text-white bg-gray-800 hover:bg-gray-900 focus:outline-none focus:ring-4 focus:ring-gray-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-gray-800 dark:hover:bg-gray-700 dark:focus:ring-gray-700 dark:border-gray-700">Report</a>
          </div>
        @else
          <div class ="flex gap-1">
            <a href="/campaigns/{{ $campaign->id }}/donate" class="focus:outline-none text-white bg-green-700 hover:bg-green-800 focus:ring-4 focus:ring-green-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-green-600 dark:hover:bg-green-700 dark:focus:ring-green-800">Donate</a>
          </div>
        @endif
      @endauth
    </div>
    <div class="mt-4 flex bg-white shadow-md p-6 rounded-lg flex-wrap gap-3">
      <h2 class="font-bold text-xl">Description</h2>
      {{ $campaign->description }}
    </div>
    <div class="mt-7 flex bg-white shadow-md p-6 rounded-lg flex-wrap gap-3">
      <h2 class="font-bold text-xl">Photo</h2>
      <img src="{{ env('BUCKET_URL') . '/' . $campaign->photo_url }}" class="w-full" alt="{{ $campaign->name }} Photo" />
    </div>

  </div>
@endsection