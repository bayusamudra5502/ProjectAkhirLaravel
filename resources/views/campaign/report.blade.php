@extends('template.master')

@section('content')
  <div class="mt-3 p-4 pt-5 rounded-lg min-h-full">
    <div class="flex flex-row justify-between">
      <h1 class="font-bold text-2xl">Campaign Report</h1>
      <a href="/campaigns/{{ $id }}/print" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800">Create PDF</a>
    </div>
    <div class="my-5 min-h-full gap-3 p-5 bg-white rounded-lg">
      <h2 class="font-bold text-xl mb-4">Report Overview</h2>
      <div class="flex flex-wrap flex-row gap-4">
        <div class="mb-4">
          <p class="font-bold mb-1">Total of Donation</p>
          <p>Rp{{ number_format($total, 0, ',', '.') }}</p>
        </div>
        <div>
          <p class="font-bold mb-1"># of Donations</p>
          <p>{{ $count }}</p>
        </div>
      </div>
    </div>

    <div class="mt-3 min-h-full gap-3 p-5 bg-white rounded-lg">
      <h2 class="font-bold text-xl mb-4">Campaign Details</h2>
      <div class="mb-4">
        <p class="font-bold mb-1">Name</p>
        <p>{{ $campaign->name }}</p>
      </div>
      <div>
        <p class="font-bold mb-1">Descriptions</p>
        <p>{{ $campaign->description }}</p>
      </div>
    </div>

    <div class="mt-3 min-h-full gap-3 p-5 bg-white rounded-lg">
      <h2 class="font-bold text-xl mb-4">Donors</h2>
  
      <div class="overflow-x-auto">
        <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
            <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                <tr>
                    <th scope="col" class="py-3 px-6">
                        Time
                    </th>
                    <th scope="col" class="py-3 px-6">
                        Donor Name
                    </th>
                    <th scope="col" class="py-3 px-6">
                        Amount
                    </th>
                </tr>
            </thead>
            <tbody>
                @forelse ($donors as $donor)
                  <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                    <td class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                      {{ $donor->created_at }}
                    </td>
                    <td class="py-4 px-6">
                        {{ $donor->name }}
                    </td>
                    <td class="py-4 px-6">
                      Rp{{ number_format($donor->amount, 0, ',', '.') }}
                    </td>
                  </tr>
                @empty
                  <tr>
                    <td colspan="3" class="text-center font-bold">
                      No Data
                    </td>
                  </tr>      
                @endforelse
            
            </tbody>
        </table>
      </div>

    </div>
  </div>
@endsection