@extends('template.master')

@section('content')
  <div class="mt-3 p-4 pt-5 rounded-lg min-h-full">
    <div class="flex flex-row justify-between">
      <h1 class="font-bold text-2xl">My Campaigns</h1>
      <a href="/campaigns/add" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800">Add Campaign</a>
    </div>
    <div class="mt-3 flex min-h-full flex-wrap gap-3">
      @forelse ($campaigns as $campaign)
        <div class="max-w-md bg-white rounded-lg border border-gray-200 shadow-md dark:bg-gray-800 dark:border-gray-700">
          <a href="/campaigns/{{ $campaign->id }}">
              <img class="rounded-t-lg" src="{{ env('BUCKET_URL') . '/' . $campaign->photo_url }}" alt="">
          </a>
          <div class="p-5">
              <a href="/campaigns/{{ $campaign->id }}">
                  <h5 class="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">{{ $campaign->name }}</h5>
              </a>
              <p class="mb-3 line-clamp-5 font-normal text-gray-700 dark:text-gray-400">{{ $campaign->description }}</p>
              <div class="flex gap-2 items-center">
                <a href="/campaigns/{{ $campaign->id }}/edit" class="inline-flex items-center py-2 px-3 text-sm font-medium text-center text-white bg-blue-700 rounded-lg hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                  Edit
                </a>
                <form method="POST" action="/campaigns/{{ $campaign->id }}">
                  @csrf
                  @method("DELETE")
                  <button type="submit" class="focus:outline-none text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:ring-red-300 font-medium rounded-lg text-sm px-4 py-2 dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-900">Delete</button>
                </form>
                <a href="/campaigns/{{ $campaign->id }}/report" class="text-white bg-gray-800 hover:bg-gray-900 focus:outline-none focus:ring-4 focus:ring-gray-300 font-medium rounded-lg text-sm px-3 py-2 dark:bg-gray-800 dark:hover:bg-gray-700 dark:focus:ring-gray-700 dark:border-gray-700">Show Report</a>
              </div>
          </div>
        </div>
      @empty
        <div class="mx-auto mt-5 text-center">
          <img class="w-32" src="{{ asset('assets/img/campaign.png') }}" alt="" />
          <p class="mt-5 text-2xl font-bold">
            Oops..
          </p>
          <p class="mt-2">There is no campaigns yet</p>
        </div>
      @endforelse
    </div>
  </div>
@endsection