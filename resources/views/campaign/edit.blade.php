@extends('template.master')

@section('content')
  <div class="mt-3 p-4 pt-5 rounded-lg">
    <div class="flex flex-row justify-between">
      <h1 class="font-bold text-2xl">Edit Campaign</h1>
    </div>
    <div class="mt-4 flex bg-white shadow-md p-6 rounded-lg flex-wrap gap-3">
      <x-auth-validation-errors class="mb-4" :errors="$errors" />

      <form action="/campaigns/{{ $campaign->id }}" method="POST" class="w-full" enctype="multipart/form-data">
        @csrf
        @method("PATCH")
        <div class="mb-4">
          <label for="name" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Campaign Name</label>
          <input type="text" value="{{ $campaign->name }}" name="name" id="name" class=" bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="New Campaign" required>
        </div>
        <div class="mb-4">
          <label for="expire" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Deadline</label>
          <input type="datetime-local" value="{{ $campaign->expired }}" name="expired" id="expire" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Deadline" required>
        </div>
        <div class="mb-4">
          <label for="description" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Description</label>
          <textarea required id="description" name="description" rows="4" class="block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="This is new camapign">{{  $campaign->description }}</textarea>
        </div>
        <div class="mb-4">
          <label class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300" for="photo">Upload Photo</label>
          <input name="photo" class="block w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 cursor-pointer dark:text-gray-400 focus:outline-none dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400" aria-describedby="user_avatar_help" id="photo" type="file">          
        </div>
        <div>
          <button type="submit" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800">Submit</button>
        </div>
      </form>
    </div>
  </div>
@endsection