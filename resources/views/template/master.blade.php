<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  @vite('resources/css/app.css')
  @stack('css')
</head>
<body class="bg-gray-100">
  <div class="flex w-full flex-col min-h-screen">
    @include('template.partials.header')
    @stack('content.before')
    <div class="container flex-1 mt-3 mx-auto p-3">
      @yield('content')
    </div>
    @stack('content.after')
    @include('template.partials.footer')
  </div>
  @vite('resources/js/app.js')
  @stack('scripts')
</body>
</html>