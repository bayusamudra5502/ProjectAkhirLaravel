@extends('template.master')

@section('content')
<div class="mt-3 p-4 pt-5 z-10">

    <div class="flex flex-row justify-between">
      <h1 class="font-bold text-3xl">{{ $profile->name }}</h1>
      @auth
        @if (Auth::user()->id === $profile->id)
          <div class ="flex gap-1">
            <a href="/profile/edit" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800">Edit Profile</a>
          </div>
        @endif
      @endauth
    </div>

    <div class="flex gap-6 flex-row mt-3 bg-white p-6 px-7 rounded-lg shadow-md">
      @if($profile->photo_url)
        <div class="w-32">
          <img class="w-full rounded-full" src="{{ env('BUCKET_URL') . '/' . $profile->photo_url }}" class="w-full" alt="{{ $profile->name }} Photo" />
        </div>
      @endif
      <div class="flex-1">
        <div class="mb-3">
          <h2 class="font-bold text-lg mb-1">Name</h2>
          <p>{{ $profile->name  }}</p>
        </div>
        <div class="mb-3">
          <h2 class="font-bold text-lg mb-1">Email</h2>
          <p>{{ $profile->email }}</p>
        </div>
        <div class="mb-3">
          <h2 class="font-bold text-lg mb-1">Birthday</h2>
          <p>{{ $profile->birthday ?? "Not Set"}}</p>
        </div>
        <div class="mb-3">
          <h2 class="font-bold text-lg mb-1">Role</h2>
          <p>{{ ucwords($profile->role) }}</p>
        </div>
      </div>
    </div>
  </div>
@endsection