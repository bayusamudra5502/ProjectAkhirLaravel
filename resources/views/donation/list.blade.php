@extends('template.master')

@section('content')
  <div class="mt-3 p-4 pt-5 rounded-lg">
    <div class="flex flex-row justify-between">
      <h1 class="font-bold text-2xl">My Donations</h1>
    </div>
    <div class="flex flex-row gap-4 mt-5">
      <div class="bg-white p-5 rounded-lg shadow-md">
        <h2 class="font-bold text-lg">Total Donation</h2>
        <p class="mt-1 text-lg">Rp{{ number_format($total, 0, ',', '.') }}</p>
      </div>
      <div class="bg-white p-5 rounded-lg shadow-md">
        <h2 class="font-bold text-lg"># of Donations</h2>
        <p class="mt-1 text-lg">{{ $number }}</p>
      </div>
    </div>

    <div class="flex flex-row justify-between">
      <h2 class="font-bold text-2xl mt-7">My Donation List</h2>
    </div>
    <div class="mt-4 flex flex-wrap gap-3">
      @forelse ($donations as $donation)
          <div class="max-w-md bg-white rounded-lg border border-gray-200 shadow-md dark:bg-gray-800 dark:border-gray-700">
              <a href="/campaigns/{{ $donation->id }}">
                  <img class="rounded-t-lg" src="{{ env('BUCKET_URL') . '/' . $donation->photo_url }}" alt="">
              </a>
              <div class="p-5">
                  <a href="/campaigns/{{ $donation->id }}">
                      <span class="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">{{ $donation->name }}</span>
                  </a>
                  <p class="my-3 line-clamp-5 font-normal text-gray-700 dark:text-gray-400">{{ $donation->description }}</p>
                  <p class="my-5 font-bold text-lg">Donation : Rp{{ number_format($donation->amount, 0, ',', '.') }}</p>
                  <div class="flex gap-2 items-center">
                    <a href="/campaigns/{{ $donation->id }}" class="inline-flex items-center py-2 px-3 text-sm font-medium text-center text-white bg-blue-700 rounded-lg hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                      Show Details
                    </a>
                  </div>
              </div>
          </div>
      @empty
          <div class="mx-auto mt-7 text-center">
          <img class="w-32" src="{{ asset('assets/logo/logo.png') }}" alt="" />
          <p class="mt-5 text-2xl font-bold">
              Oops..
          </p>
          <p class="mt-2">There is no donation yet</p>
          </div>
      @endforelse
    </div>
  </div>
@endsection