@extends('template.master')

@section('content')
    <div class="flex flex-row justify-between">
      <h2 class="font-bold text-2xl mt-7">Give Donation</h2>
    </div>
    <div class="mt-4 flex bg-white shadow-md p-6 rounded-lg flex-wrap gap-3">
      <x-auth-validation-errors class="mb-4" :errors="$errors" />

      <form action="/campaigns/{{ $id }}/donate" method="POST" class="w-full" enctype="multipart/form-data">
        @csrf
        <div class="mb-4">
          <label for="donation" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Amount</label>
          <input type="number" name="amount" id="donation" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="20000" required>
        </div>
        <div>
          <button type="submit" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800">Submit</button>
        </div>
      </form>
    </div>
@endsection