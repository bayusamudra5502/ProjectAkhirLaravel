<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('campaigns', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::table('needs', function (Blueprint $table) {
            $table->foreign('campaign_id')->references('id')->on('campaigns')
                ->cascadeOnDelete()
                ->cascadeOnUpdate();
        });

        Schema::table('donations', function (Blueprint $table) {
            $table->foreign('campaign_id')->references('id')->on('campaigns')
                ->cascadeOnDelete()
                ->cascadeOnUpdate();
            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::table('prayers', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('campaign_id')->references('id')->on('campaigns')
                ->cascadeOnDelete()
                ->cascadeOnUpdate();
        });

        Schema::table('withdrawals', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('campaign_id')->references('id')->on('campaigns')
                ->cascadeOnDelete()
                ->cascadeOnUpdate();
            // $table->foreign('account_number')->references('account_number')->on('accounts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('campaigns', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
        });

        Schema::table('needs', function (Blueprint $table) {
            $table->dropForeign(['campaign_id']);
        });

        Schema::table('donations', function (Blueprint $table) {
            $table->dropForeign(['campaign_id']);
            $table->dropForeign(['user_id']);
        });

        Schema::table('prayers', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
            $table->dropForeign(['campaign_id']);
        });

        Schema::table('withdrawals', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
            $table->dropForeign(['campaign_id']);
            $table->dropForeign(['account_number']);
        });
    }
};
