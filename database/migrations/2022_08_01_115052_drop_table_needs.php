<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('needs');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('needs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('campaign_id');
            $table->enum('type', ['money', 'rice', 'cloth']);
            $table->integer('amount');
            $table->timestamps();
            $table->unique(['campaign_id','type']);
            $table->foreign('campaign_id')->references('id')->on('campaigns');
        });
    }
};
