<?php

use App\Http\Controllers\Auth\ProfileController;
use Illuminate\Support\Facades\Route;

Route::middleware('auth')->group(function () {
  Route::get("profile", [ProfileController::class, 'index']);
  Route::get("profile/edit", [ProfileController::class, 'edit']);
  Route::patch("profile", [ProfileController::class, 'update']);
});

Route::get("profile/{id}", [ProfileController::class, 'show']);
