<?php

use App\Http\Controllers\Campaign\CampaignController;
use Illuminate\Support\Facades\Route;

Route::middleware('role.fundraiser')->group(function () {
  Route::get('campaigns/dashboard', [CampaignController::class, 'list_own']);
  Route::get('campaigns/{id}/edit', [CampaignController::class, 'edit']);
  Route::get('campaigns/add', [CampaignController::class, 'add']);
  Route::get('campaigns/{id}/report', [CampaignController::class, 'show_report']);
  Route::get('campaigns/{id}/print', [CampaignController::class, 'print_report']);
  Route::post('campaigns', [CampaignController::class, 'store']);
  Route::delete('campaigns/{id}', [CampaignController::class, 'destroy']);
  Route::patch('campaigns/{id}', [CampaignController::class, 'update']);
});

Route::get('campaigns', [CampaignController::class, 'index']);
Route::get('campaigns/{id}', [CampaignController::class, 'show']);