<?php

use App\Http\Controllers\Campaign\DonationController;
use Illuminate\Support\Facades\Route;

Route::middleware('auth')->group(function () {
  Route::get('donations', [DonationController::class, 'index']);
  Route::get('campaigns/{id}/donate', [DonationController::class, 'add']);
  Route::post('campaigns/{id}/donate', [DonationController::class, 'store']);
});
