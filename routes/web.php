<?php

use App\Models\Campaign;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $campaigns = Campaign::orderBy('id', 'DESC')->limit(10)->get();
    return view('welcome', compact('campaigns'));
});

Route::get('/dashboard', function (Request $req) {
    if($req->user()->role === 'fundraiser') {
        return redirect('/campaigns/dashboard');
    } else {
        return redirect('/donations');
    }
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';
require __DIR__.'/campaigns.php';
require __DIR__.'/profile.php';
require __DIR__.'/donation.php';
