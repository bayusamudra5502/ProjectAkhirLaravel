# Project Akhir Sanbercode

Ini adalah repository kami

## Anggota Kelompok

1. Bayu Samudra
2. Muhammad Ikhsan Rizki Pratama
3. Imam Sujatmiko

## Video Youtube

https://youtu.be/FVv2bX_ROTk

## Deployment

https://laravel-tugas-akhir.herokuapp.com/

Note:

-   Jangan lupa setup env `BUCKET_URL` sebagai URL depan.
